<?php
class Movies extends CI_Controller {

    public $moviesArr = array();

	function __construct()
	{
		parent::__construct();
        $this->movieAPICall();

	}

	private function movieAPICall(){

        $movies = array();

	    $session = curl_init("https://backend-ygzsyibiue.now.sh/api/v1/movies/");
        curl_setopt($session, CURLOPT_POST, False);
        curl_setopt($session, CURLOPT_HEADER, False);
        curl_setopt($session, CURLOPT_RETURNTRANSFER, True);
        curl_setopt($session, CURLOPT_VERBOSE, True);
        curl_setopt($session, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($session, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($session, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        $movies = curl_exec($session); // CURL call to get all the articles

        $movies = json_decode($movies, true); // decode the JSON to make associative array

        foreach($movies as $key => $value){
            $this->moviesArr[$value['slug']] = $value;
        }

        return $this->moviesArr;
}

    public function index()
    {

        $data['error'] = '';
        $data['message'] = '';

        $data['movies'] = $this->moviesArr;

        $this->smarty->assign("title", 'Cimpress');
        $this->smarty->assign("name", 'Movies Listing Page');

        $this->smarty->view("movies_list.tpl", $data);


    }

    public function movieDetails($slug=NULL){
        $movie = $this->moviesArr[$slug];

        $this->smarty->assign("movieTitle", $movie['title']);
        $this->smarty->assign("moviePosterURL", $movie['posterURL']);
        $this->smarty->assign("movieBackdropURL", $movie['backdropURL']);
        $this->smarty->assign("movieReleaseDate", $movie['releaseDate']);
        $this->smarty->assign("moviePlot", $movie['plot']);

        $this->smarty->assign("title", 'Cimpress');
        $this->smarty->assign("name", $movie['title']);

        $this->smarty->view("movie_details.tpl");

    }
}
