<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>{$title} - {$name}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link href="{site_url('assets/css/index.min.css')}" rel="stylesheet" id="index-css">

</head>

<div class="container">
    <div class="row">
        <section id="pinBoot">

            {foreach $movies as $key => $movie}

                <article class="white-panel">
                    <a href="{site_url('movies/movieDetails/')}{$movie['slug']}">
                    <img src="https://image.tmdb.org/t/p/w500{$movie['posterURL']}" alt="{$movie['title']}">
                    </a>
                    <h4>
                        <a href="{site_url('movies/movieDetails/')}{$movie['slug']}">{$movie['title']}</a>
                    </h4>
                    <h4>
                        {$movie['releaseDate']|date_format}
                    </h4>
                </article>

            {/foreach}

        </section>
    </div>

</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="{site_url('assets/js/index.min.js')}"></script>

</body>
</html>