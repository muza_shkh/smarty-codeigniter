<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>{$title} | {$name}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">

    <style>
        .backdrop {
            background-image: url(https://image.tmdb.org/t/p/w1280{$movieBackdropURL});
            background-repeat: no-repeat;
            background-position: center center;
            background-size: cover;
            background-attachment: fixed;
            height: 60vh;
        }

        .poster{
            height:500px;
            left:150px; /* play around with this */
            position:absolute;
            top:200px; /* and play around with this */
            width:200px;
        }

        .details{
            left:400px;
            position:absolute;
        }

        .plot{
            left:150px;
            text:wrap;
            padding-top: 150px;
            position:absolute;

        }
    </style>

</head>
<body>
<div class="backdrop"></div>
<div class="poster" style="">
    <img src="https://image.tmdb.org/t/p/w500{$moviePosterURL}" alt="" heigth="350px" width="200px">
</div>
<div class="details">
    <h1>
        {$title}
    </h1>
    <h4>
        {$movieReleaseDate|date_format:"%A, %B %e, %Y"}
    </h4>
</div>
<div class="plot">
    <p>{$moviePlot}</p>
    <hr>
    <a href="{site_url('movies')}">Back to Homepage</a>
</div>

<body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>

</body>
</html>