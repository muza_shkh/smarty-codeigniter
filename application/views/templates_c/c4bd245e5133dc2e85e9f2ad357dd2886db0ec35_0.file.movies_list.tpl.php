<?php
/* Smarty version 3.1.33, created on 2019-01-21 12:47:01
  from '/Users/muzaffarshaikh/www/cimpress/application/views/templates/movies_list.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5c45b1354f7d96_04654197',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c4bd245e5133dc2e85e9f2ad357dd2886db0ec35' => 
    array (
      0 => '/Users/muzaffarshaikh/www/cimpress/application/views/templates/movies_list.tpl',
      1 => 1548071216,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c45b1354f7d96_04654197 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'/Users/muzaffarshaikh/www/cimpress/application/third_party/smarty/libs/plugins/modifier.date_format.php','function'=>'smarty_modifier_date_format',),));
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
 - <?php echo $_smarty_tpl->tpl_vars['name']->value;?>
</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link href="<?php echo site_url('assets/css/index.min.css');?>
" rel="stylesheet" id="index-css">

</head>

<div class="container">
    <div class="row">
        <section id="pinBoot">

            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['movies']->value, 'movie', false, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['movie']->value) {
?>

                <article class="white-panel">
                    <a href="<?php echo site_url('movies/movieDetails/');
echo $_smarty_tpl->tpl_vars['movie']->value['slug'];?>
">
                    <img src="https://image.tmdb.org/t/p/w500<?php echo $_smarty_tpl->tpl_vars['movie']->value['posterURL'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['movie']->value['title'];?>
">
                    </a>
                    <h4>
                        <a href="<?php echo site_url('movies/movieDetails/');
echo $_smarty_tpl->tpl_vars['movie']->value['slug'];?>
"><?php echo $_smarty_tpl->tpl_vars['movie']->value['title'];?>
</a>
                    </h4>
                    <h4>
                        <?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['movie']->value['releaseDate']);?>

                    </h4>
                </article>

            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

        </section>
    </div>

</div>
<?php echo '<script'; ?>
 src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="https://code.jquery.com/jquery-1.11.1.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo site_url('assets/js/index.min.js');?>
"><?php echo '</script'; ?>
>

</body>
</html><?php }
}
