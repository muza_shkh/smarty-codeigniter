<?php
/* Smarty version 3.1.33, created on 2019-01-21 12:48:46
  from '/Users/muzaffarshaikh/www/cimpress/application/views/templates/movie_details.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5c45b19ec960d6_43259660',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '4da838ae437d0e8b62587a59d1698c3268520d63' => 
    array (
      0 => '/Users/muzaffarshaikh/www/cimpress/application/views/templates/movie_details.tpl',
      1 => 1548071323,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c45b19ec960d6_43259660 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'/Users/muzaffarshaikh/www/cimpress/application/third_party/smarty/libs/plugins/modifier.date_format.php','function'=>'smarty_modifier_date_format',),));
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
 | <?php echo $_smarty_tpl->tpl_vars['name']->value;?>
</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">

    <style>
        .backdrop {
            background-image: url(https://image.tmdb.org/t/p/w1280<?php echo $_smarty_tpl->tpl_vars['movieBackdropURL']->value;?>
);
            background-repeat: no-repeat;
            background-position: center center;
            background-size: cover;
            background-attachment: fixed;
            height: 60vh;
        }

        .poster{
            height:500px;
            left:150px; /* play around with this */
            position:absolute;
            top:200px; /* and play around with this */
            width:200px;
        }

        .details{
            left:400px;
            position:absolute;
        }

        .plot{
            left:150px;
            text:wrap;
            padding-top: 150px;
            position:absolute;

        }
    </style>

</head>
<body>
<div class="backdrop"></div>
<div class="poster" style="">
    <img src="https://image.tmdb.org/t/p/w500<?php echo $_smarty_tpl->tpl_vars['moviePosterURL']->value;?>
" alt="" heigth="350px" width="200px">
</div>
<div class="details">
    <h1>
        <?php echo $_smarty_tpl->tpl_vars['title']->value;?>

    </h1>
    <h4>
        <?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['movieReleaseDate']->value,"%A, %B %e, %Y");?>

    </h4>
</div>
<div class="plot">
    <p><?php echo $_smarty_tpl->tpl_vars['moviePlot']->value;?>
</p>
    <hr>
    <a href="<?php echo site_url('movies');?>
">Back to Homepage</a>
</div>

<body>
<?php echo '<script'; ?>
 src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="https://code.jquery.com/jquery-1.11.1.min.js"><?php echo '</script'; ?>
>

</body>
</html><?php }
}
